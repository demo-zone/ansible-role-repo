# Repositories role
Role to configuring repositories available required.

## Role variables
| variable                   | default             | Description  |
| -------------------------- | ------------------- | ------------ |
| repo_filename              |                     | File name    |
| repos                      | []                  | Repos info   |



## Example

### Add CentOS7 repos

Add EPEL repos on `myrepository.repo`

```
- hosts: localhost
  vars:
    repo_filename: myrepository
    repos:
      - label: epel
        name: Extra Packages for Enterprise Linux 7 - x86_64
        url: download.fedoraproject.org/pub/epel/7/x86_64
  roles:
    - role: repo
      
```

## Role tests

Review `.gitlab-ci.yml` to select base image and use "OS[version]" as arguments: 
```
# Test role in CentOS7
docker run --rm -v `pwd`:/roleproject ansible/centos7-ansible /bin/bash -c 'cd /roleproject/; tests/.test-suite CentOS7'

# Test role in OL7
docker run --rm -v `pwd`:/roleproject kikicarbonell/ansible-oel7:ansible-2.7.8 /bin/bash -c 'cd /roleproject/; tests/.test-suite OL7'

```

